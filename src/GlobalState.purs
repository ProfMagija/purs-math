module GlobalState where

import Prelude

import Effect (Effect)

-- | A effectful global state type
foreign import data GlobalState :: Type -> Type

-- | Creates the global state, given it's initial value
foreign import mkGlobalState :: forall a. a -> GlobalState a

-- | Sets the global state to a new value, and returns the new value
foreign import setState :: forall a. GlobalState a -> a -> Effect a

-- | Gets the value of the global state
foreign import getState :: forall a. GlobalState a -> Effect a

-- | Modifies the state using `fn`, as in `newstate <- fn oldstate`
-- | and returns the new value
modState :: forall a. GlobalState a -> (a -> a) -> Effect a
modState state fn = do
  v <- getState state
  setState state $ fn v