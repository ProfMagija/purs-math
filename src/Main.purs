module Main where

import Data.Array ((:), reverse)
import Data.Array.Partial (tail, head)
import Data.Int (toNumber)
import Data.String (joinWith)
import Data.Tuple (Tuple(..), uncurry)
import Effect (Effect)
import GlobalState (GlobalState, mkGlobalState, modState)
import MathPurs.Animate (Animation(..), execute)
import MathPurs.Canvas (blue, canvas, circle, clearRect, draw, green, line, poly, red)
import MathPurs.Forms (button, htmlPut, label, newline, style, table)
import MathPurs.Katex (render)
import Partial.Unsafe (unsafePartial)
import Prelude (Unit, bind, discard, show, ($), (<>))
import Web.HTML (HTMLElement)


display :: HTMLElement
display = label ""

currentFormula :: GlobalState (Tuple (Array String) (Array String))
currentFormula = mkGlobalState $ Tuple [] []

form :: HTMLElement
form = table [
  [ button "C" clear, button "√" putSqrt, button "÷" putFrac, button "×" $ putTimes ],
  [ button "7" $ putNum 7, button "8" $ putNum 8, button "9" $ putNum 9, button "-" putMinus ],
  [ button "4" $ putNum 4, button "5" $ putNum 5, button "6" $ putNum 6, button "+" putPlus ],
  [ button "1" $ putNum 1, button "2" $ putNum 2, button "3" $ putNum 3 ],
  [ button "0" $ putNum 0, button "<-" $ moveLeft, button "->" $ moveRight]
] where 

  mkFormula :: (Tuple (Array String) (Array String)) -> String
  mkFormula (Tuple before after) = 
    joinWith "|" [ joinWith "" $ reverse before, joinWith "" after ]

  modFormula :: ((Array String) -> (Array String) -> (Tuple (Array String) (Array String))) -> Effect Unit
  modFormula fn = do
    formula <- modState currentFormula $ uncurry fn
    --log $ mkFormula formula
    render (mkFormula formula) display

  addFormula before after = modFormula $ \b a -> Tuple (before <> b) (after <> a)

  clear = modFormula $ \_ _ -> (Tuple [] [])
    
  putSqrt = addFormula ["\\sqrt{"] ["}"]
    
  putFrac = addFormula ["\\frac{"] ["}{", "}"]
    
  putStr s = modFormula $ \b a -> Tuple (s : b) a
  putTimes = putStr "\\cdot{}"
  putMinus = putStr "-"
  putPlus = putStr "+"
  putNum n = putStr $ show n

  moveLeft = modFormula $ move

  moveRight = modFormula $ \x y -> swap (move y x)

  move [] a = Tuple [] a
  move b a = unsafePartial $ Tuple (tail b) ((head b):a)

  swap (Tuple a b) = Tuple b a

pt :: Int -> Int -> Tuple Number Number
pt x y = Tuple (toNumber x) (toNumber y)

reorderedFormula :: Effect Unit
reorderedFormula = do
  htmlPut $ formula1
  htmlPut $ formula2
  htmlPut $ formula3
  htmlPut $ formula4

  -- this makes the formula4 hidden
  style "width" "0" formula4
  style "opacity" "0" formula4
  style "display" "inline-block" formula4

  htmlPut $ button "click me" $ anim

  render "x" formula1
  render "- 1" formula2
  render "= 2" formula3
  render "+ 1" formula4
  where
    formula1 = label ""
    formula2 = label "" 
    formula3 = label ""
    formula4 = label ""
    anim = do
      execute (Parallel [
        Animate "opacity" "1.0" 1000,
        Animate "width" "30" 1000
      ]) formula4
      execute (Parallel [
        Animate "opacity" "0" 1000,
        Animate "width" "0" 1000
      ]) formula2
      

main :: Effect Unit
main = do
  htmlPut display
  htmlPut newline
  htmlPut form
  htmlPut $ cvs
  htmlPut newline
  htmlPut $ button "magic" $ draw cvs [clearRect 0.0 0.0 300.0 300.0]
  htmlPut $ button "more magic" $ draw cvs drawing
  htmlPut $ button "even more magic" $ execute (Animate "left" "+=50" 1000) cvs
  htmlPut newline
  reorderedFormula


  where
    cvs = canvas 300.0 300.0

    drawing = [
      blue, circle false 150.0 150.0 75.0,
      green, line 50.0 250.0 250.0 50.0, line 50.0 50.0 250.0 250.0,
      red, poly true [pt 150 100, pt 200 150, pt 150 200, pt 100 150]
    ]
