exports.mkGlobalState = function (a) {
    return { val: a };
}

exports.getState = function (h) {
    return function () {
        return h.val;
    }
}

exports.setState = function (h) {
    return function (v) {
        return function () {
            return h.val = v;
        }
    }
}