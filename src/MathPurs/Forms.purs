module MathPurs.Forms (
    table, label, textbox, button, value, htmlPut, newline, style
) where

import Prelude

import Data.Maybe (Maybe)
import Data.Nullable (Nullable, toMaybe)
import Effect (Effect)
import Web.HTML.HTMLElement (HTMLElement)

-- | creates an HTML table, given an array of arrays of it's cells
foreign import table :: Array (Array HTMLElement) -> HTMLElement

-- | creates a textual label (as a HTML `<div>` element)
foreign import label :: String -> HTMLElement

-- | creates an HTML button, with the given label and on-click action
foreign import button :: String -> Effect Unit -> HTMLElement

-- | creates a textbox with a given on-modify action
foreign import textbox :: Effect Unit -> HTMLElement

-- | "puts" the given HTMLElement into the `<body>` of the page
foreign import htmlPut :: HTMLElement -> Effect Unit

foreign import newline :: HTMLElement

foreign import _value :: HTMLElement -> Nullable String

foreign import style :: String -> String -> HTMLElement -> Effect Unit

-- | gets the value from the HTMLElement
value :: HTMLElement -> Maybe String
value el = toMaybe $ _value el

