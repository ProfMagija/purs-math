
exports.table = function(rows) {
    var tbl = document.createElement("table");
    rows.forEach(function (row) {
        var r = document.createElement("tr");
        row.forEach(function (cell) {
            var c = document.createElement("td");
            c.appendChild(cell);
            r.appendChild(c);
        });
        tbl.appendChild(r);
    });
    return tbl;
}

exports.label = function(s) {
    var l = document.createElement("span");
    l.innerText = s;
    return l;
}

exports.button = function (label) {
    return function (handler) {
        var b = document.createElement("button");
        b.innerText = label;
        b.addEventListener('click', function () { handler(); });
        return b;
    }
}

exports.textbox = function (handler) {
    var b = document.createElement("input")
    b.type = "text";
    b.addEventListener('change', function () { handler(); });
    return b;
}

exports._value = function (x) {
    return x.value || null;
}

exports.htmlPut = function (el) {
    return function() {
        document.body.appendChild(el);
    }
}

exports.style = function (p) {
    return function (s) {
        return function (el) {
            return function () {
                el.style[p] = s;
            }
        }
    }
}

exports.newline = document.createElement("br");