"use strict";

exports.rawKatexRender = function(opts) {
    return function (s) {
        return function (e) {
            return function () {
                katex.render(s, e, opts);
            };
        };
    };
}

exports.rawKatexRenderToString = function(opts) {
    return function (s) {
        return katex.renderToString(s, opts);
    };
}

exports._getElementById = function (id) {
    return document.getElementById(id);
};