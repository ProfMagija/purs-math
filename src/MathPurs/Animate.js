
exports._animate = function (el) {
    return function (args) {
        return function (dur) {
            return function (cb) {
                return function () {
                    var po = {};
                    for (var i = 0; i < args.length; i += 2) {
                        po[args[i]] = args[i+1];
                    }
                    $(el).animate(po, {
                        queue: false,
                        duration: dur,
                        complete: cb
                    })
                }
            }
        }
    }
}

exports._cbJoin = function (n) {
    return function (cb) {
        return function () {
            var i = n;
            return function () {
                i--;
                if (i == 0) {
                    cb();
                }
            }
        }
    }
}