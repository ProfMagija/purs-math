module MathPurs.Animate (
    Animation(..), execute
) where

import Prelude

import Data.Array (foldr, length)
import Data.Foldable (traverse_)
import Effect (Effect)
import Web.HTML (HTMLElement)

data Animation = Animate String String Int
               | Parallel (Array Animation)
               | Sequential (Array Animation)

foreign import _animate :: HTMLElement -> Array String -> Int -> Effect Unit -> Effect Unit
foreign import _cbJoin :: Int -> Effect Unit -> Effect (Effect Unit)

_execute :: Effect Unit -> Animation -> HTMLElement -> Effect Unit
_execute cb (Animate p v i) el = _animate el [p, v] i cb
_execute cb (Parallel anims) el = do
    cbj <- _cbJoin (length anims) cb
    traverse_ (\an -> _execute cbj an el) anims
_execute cb (Sequential anims) el =
    foldr (\an c -> _execute c an el) cb anims

execute :: Animation -> HTMLElement -> Effect Unit
execute = _execute $ pure unit
