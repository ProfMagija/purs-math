
exports.canvas = function(x) {
    return function (y) {
        var c = document.createElement("canvas");
        c.width = x;
        c.height = y;
        return c;
    }
}

exports.clearRect = function (x) {
    return function (y) {
        return function (w) {
            return function (h) {
                return function (ctx) {
                        ctx.clearRect(x, y, w, h);
                    }
                }
            }
        }
    }

exports.fillRect = function (x) {
    return function (y) {
        return function (w) {
            return function (h) {
                return function (ctx) {
                        ctx.fillRect(x, y, w, h);
                    }
                }
            }
        }
    }

exports.strokeRect = function (x) {
    return function (y) {
        return function (w) {
            return function (h) {
                return function (ctx) {
                    ctx.strokeRect(x, y, w, h);
                }
            }
        }
    }
}


exports.fillText = function (s) {
    return function (x) {
        return function (y) {
            return function (ctx) {
                ctx.fillText(s, x, y);
            }
        }
    }
}


exports.strokeText = function (s) {
    return function (x) {
        return function (y) {
            return function (ctx) {
                ctx.strokeText(s, x, y);
            }
        }
    }
}


exports.lineWidth = function (w) {
    return function (ctx) {
        ctx.lineWidth = w;
    }
}


exports.font = function (f) {
    return function (ctx) {
        ctx.font = f;
    }
}


exports._color = function (f) {
    return function (ctx) {
        ctx.fillStyle = f;
        ctx.strokeStyle = f;
    }
}


exports._path = function (fill) {
    return function (p) {
        return function (ctx) {
            if (p.length < 2) 
                return;
            ctx.beginPath();
            ctx.moveTo(p[0][0], p[0][1]);
            for (var i = 1; i < p.length; i++) {
                ctx.lineTo(p[i][0], p[i][1]);
            }
            if (fill) {
                ctx.closePath();
                ctx.fill();
            }
            else
                ctx.stroke();
        }
    }
}

exports._ellipse = function (fill) {
    return function (ar) {
        return function (ctx) {
            ctx.beginPath();
            if (ar.length == 7)
                ctx.ellipse(ar[0], ar[1], ar[2], ar[3], ar[4], ar[5], ar[6]);
            if (fill)
                ctx.fill();
            else
                ctx.stroke();
        }
    }
}

exports.combine = function (effs) {
    return function (ctx) {
        for (var i = 0; i < effs.length; i++) {
            effs[i](ctx);
        }
    }
}

exports.nothing = function (ctx) {}

exports.draw = function (elem) {
    return function (effs) {
        return function () {
            var ctx = elem.getContext && elem.getContext('2d');
            if (ctx) exports.combine(effs)(ctx);
        }
    }
}