module MathPurs.Katex 
    ( KatexOptions
    , defaultOptions
    , render
    , renderToString
    , renderDisplay
    , renderDisplayToString
    , renderOpt
    , renderToStringOpt
    , getElementById
    -- , onLoad
    ) where

import Data.Maybe (Maybe)
import Data.Nullable (Nullable, toMaybe)
import Effect (Effect)
import Global (infinity)
import Prelude (Unit, ($))
import Web.HTML.HTMLElement (HTMLElement)

-- | options for Katex rendering
type KatexOptions = 
    { displayMode :: Boolean
    , throwOnError :: Boolean
    , errorColor :: String
    , colorIsTextColor :: Boolean
    , maxSize :: Number
    , maxExpand :: Number
    }

-- | defalut "inline" options
defaultOptions :: KatexOptions
defaultOptions = 
    { displayMode: false
    , throwOnError: false
    , errorColor: "#cc0000"
    , colorIsTextColor: false
    , maxSize: infinity
    , maxExpand: infinity
    }

-- | default display options
displayOptions :: KatexOptions
displayOptions = defaultOptions { displayMode = true }

foreign import rawKatexRender 
    :: KatexOptions
    -> String 
    -> HTMLElement
    -> Effect Unit

foreign import rawKatexRenderToString
    :: KatexOptions
    -> String
    -> String

-- | renders the given LaTeX string to a HTML element
render :: String -> HTMLElement -> Effect Unit
render = rawKatexRender defaultOptions

-- | returns the string HTML representation of the given LaTeX string
renderToString :: String -> String
renderToString = rawKatexRenderToString defaultOptions

-- | renders the given LaTeX string to a HTML element, with display-math options
renderDisplay :: String -> HTMLElement -> Effect Unit
renderDisplay = rawKatexRender displayOptions

-- | returns the string HTML representation of the given LaTeX string, with display-math options
renderDisplayToString :: String -> String
renderDisplayToString = rawKatexRenderToString displayOptions

-- | renders the given LaTeX string to a HTML element, with options specified
renderOpt :: KatexOptions -> String -> HTMLElement -> Effect Unit
renderOpt = rawKatexRender

-- | returns the string HTML representation of the given LaTeX string, with options specified
renderToStringOpt :: KatexOptions -> String -> String
renderToStringOpt = rawKatexRenderToString

foreign import _getElementById
    :: String 
    -> Nullable HTMLElement

-- | Gets the element by specified html id
getElementById :: String -> Maybe HTMLElement
getElementById id = toMaybe $ _getElementById id

-- foreign import _onLoad
--     :: (HTMLDocument -> Effect Unit)
--     -> Effect Unit

-- onLoad :: (HTMLDocument -> Effect Unit) -> Effect Unit
-- onLoad = _onLoad