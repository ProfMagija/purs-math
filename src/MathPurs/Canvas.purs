module MathPurs.Canvas where

import Prelude

import Data.Array (unsafeIndex)
import Data.String (joinWith)
import Data.Tuple (Tuple, fst, snd)
import Effect (Effect)
import Math (pi)
import Partial.Unsafe (unsafePartial)
import Web.HTML (HTMLElement)

-- | Creates a canvas, given dimensions
foreign import canvas :: Number -> Number -> HTMLElement

foreign import data Graphics :: Type

foreign import clearRect :: Number -> Number -> Number -> Number -> Graphics
foreign import fillRect :: Number -> Number -> Number -> Number -> Graphics
foreign import strokeRect :: Number -> Number -> Number -> Number -> Graphics

foreign import fillText :: String -> Number -> Number -> Graphics
foreign import strokeText :: String -> Number -> Number -> Graphics

foreign import lineWidth :: Number -> Graphics

foreign import font :: String -> Graphics

foreign import _color :: String -> Graphics

rgb :: Number -> Number -> Number -> Graphics
rgb r g b = _color $ "rgb(" <> joinWith "," [f r, f g, f b] <> ")"
    where 
        f x | x < 0.0   = "0"
            | x > 255.0 = "255"
            | otherwise = show x

hsl :: Number -> Number -> Number -> Graphics
hsl h s l = _color $ "hsl(" <> joinWith "," [f 0.0 360.0 h, f 0.0 1.0 s, f 0.0 1.0 l] <> ")"
    where 
        f min max x | x < min   = show min
                    | x > max   = show max
                    | otherwise = show x

black :: Graphics
black = _color "black"
silver :: Graphics
silver = _color "silver"
gray :: Graphics
gray = _color "gray"
white :: Graphics
white = _color "white"
maroon :: Graphics
maroon = _color "maroon"
red :: Graphics
red = _color "red"
purple :: Graphics
purple = _color "purple"
fuchsia :: Graphics
fuchsia = _color "fuchsia"
green :: Graphics
green = _color "green"
lime :: Graphics
lime = _color "lime"
olive :: Graphics
olive = _color "olive"
yellow :: Graphics
yellow = _color "yellow"
navy :: Graphics
navy = _color "navy"
blue :: Graphics
blue = _color "blue"
teal :: Graphics
teal = _color "teal"
aqua :: Graphics
aqua = _color "aqua"
orange :: Graphics
orange = _color "orange"

foreign import _path :: Boolean -> Array (Array Number) -> Graphics
foreign import _ellipse :: Boolean -> Array Number -> Graphics

ellipse :: Boolean -> Number -> Number -> Number -> Number -> Number -> Number -> Number -> Graphics
ellipse fill x y rx ry rot startAngle stopAngle = _ellipse fill [x, y, rx, ry, rot, startAngle, stopAngle]

circle :: Boolean -> Number -> Number -> Number -> Graphics
circle fill x y r = _ellipse fill [x, y, r, r, 0.0, 0.0, 2.0 * pi]

arc :: Boolean -> Number -> Number -> Number -> Number -> Number -> Graphics
arc fill x y r startAngle stopAngle = _ellipse fill [x, y, r, r, 0.0, startAngle, stopAngle]

line :: Number -> Number -> Number -> Number -> Graphics
line x1 y1 x2 y2 = _path false [[x1, y1], [x2, y2]]

segments :: Boolean -> Array (Tuple Number Number) -> Graphics
segments filled points = _path filled $ map (\x -> [fst x, snd x]) points

poly :: Boolean -> Array (Tuple Number Number) -> Graphics
poly _ [] = nothing
poly _ [_] = nothing
poly filled points  = unsafePartial $ segments filled (points <> [unsafeIndex points 0])

filledPoly :: Array (Tuple Number Number) -> Graphics
filledPoly = poly true

foreign import combine :: Array Graphics -> Graphics
foreign import nothing :: Graphics

foreign import draw :: HTMLElement -> Array Graphics -> Effect Unit
