module MathPurs.Expression (Expression(..), BinaryOperator(..), UnaryOperator(..), toLatex) where
  
import Prelude

data BinaryOperator = Plus
                    | Minus
                    | Times
                    | Div

data UnaryOperator = UnaryPlus
                   | UnaryMinus
                   | SquareRoot

data Expression = Placeholder
                | Incomplete Expression
                | BinOp BinaryOperator Expression Expression
                | UnaryOp UnaryOperator Expression
                | Num Number
                | Var String

toLatex :: Expression -> String
toLatex (Incomplete ex) = toLatex ex <> "|"
toLatex (BinOp op lhs rhs) = binopToLatex op lhs rhs
toLatex (UnaryOp op arg) = unaryopToLatex op arg
toLatex (Num n) = show n
toLatex (Var s) = s
toLatex Placeholder = "|"


binopToLatex :: BinaryOperator -> Expression -> Expression -> String
binopToLatex Plus = binopHelper "" "+" ""
binopToLatex Minus = binopHelper "" "-" ""
binopToLatex Times = binopHelper "" "\\cdot{}" ""
binopToLatex Div = binopHelper "\\frac{" "}{" "}"

binopHelper :: String -> String -> String -> Expression -> Expression -> String
binopHelper before middle after lhs rhs = before <> toLatex lhs <> middle <> toLatex rhs <> after

unaryopToLatex :: UnaryOperator -> Expression -> String
unaryopToLatex UnaryPlus = unopHelper "+" ""
unaryopToLatex UnaryMinus = unopHelper "-" ""
unaryopToLatex SquareRoot = unopHelper "\\sqrt{" "}"

unopHelper :: String -> String -> Expression -> String
unopHelper before after arg = before <> toLatex arg <> after