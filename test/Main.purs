module Test.Main where

import Prelude
import MathPurs.Katex
import Effect (Effect)
import Effect.Console (log)

main :: Effect Unit
main = do
  log $ renderToString "a^2"
